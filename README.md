# K40 Whisperer mirror

This is a mirror of [K40 Whisperer project](https://www.scorchworks.com/K40whisperer/k40whisperer.html) by [Scorchworks](http://www.scorchworks.com/)

## Mirror script
A script for mirroring this project resides in mirror folder  
This script will dowload and extract zip file contents in the project folder
Then it converts text change log file to MD format
and renames licence file  
Usage:
```bash
cd mirror
./grab_latest.sh
```
