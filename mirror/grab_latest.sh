#!/bin/bash
fname=$(curl -s https://www.scorchworks.com/K40whisperer/k40whisperer.html | grep K40_Whisperer_*\.*src.zip -m1|cut -d'"' -f2)
if [[ ! -z "$fname" ]]
then
	wget https://www.scorchworks.com/K40whisperer/$fname -P /tmp/
	unzip /tmp/$fname -d /tmp
	mv /tmp/$(basename $fname .zip)/* ..
	rm -rf /tmp/$(basename $fname .zip) /tmp/$fname
fi

echo '# K40 Whisperer Changelog' > ../CHANGELOG.md
cat ../Change_Log.txt |sed -E 's/^(Version.*):/## \1/' >> ../CHANGELOG.md
rm ../Change_Log.txt
mv ../gpl-3.0.txt ../LICENSE
mv ../README_Linux.txt ../README_Linux.md
